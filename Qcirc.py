from qiskit import ClassicalRegister, QuantumRegister, Aer, QuantumCircuit, execute, IBMQ
from qiskit.providers.aer import noise
from warnings import filterwarnings
import os
filterwarnings("ignore")

def qctxbank(circuit, device, coupling_map):
    properties = device.properties()
    noise_model = noise.device.basic_device_noise_model(properties)
    basis_gates = noise_model.basis_gates
    backend = Aer.get_backend('qasm_simulator')
    job_sim = execute(circuit, backend,
                  coupling_map=coupling_map,
                  noise_model=noise_model,
                  basis_gates=basis_gates)
    sim_result = job_sim.result()
    return sim_result.get_counts(circuit)

MY_TOKEN = os.environ['MY_TOKEN']
IBMQ.save_account(MY_TOKEN)
IBMQ.load_account()
provider = IBMQ.get_provider(group='open')
device = provider.get_backend('ibmqx2')
coupling_map2=[[0, 1], [1, 2], [2, 3], [3, 4], [4, 5]]

bank = []

for i in range(5):
    q = QuantumRegister(1)
    c = ClassicalRegister(1)
    circuit = QuantumCircuit(q, c)
    coupling_map1=[0, i + 1]
    circuit.h(q)
    circuit.measure(q, c)
    QT1 = qctxbank(circuit, device, coupling_map1)
    bank.append(abs(QT1['1']-QT1['0']))

print(bank)
'''f = open("quantum.txt","w")
f.write(str(bank))
f.close()'''
