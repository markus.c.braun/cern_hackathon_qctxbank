from qiskit import ClassicalRegister, QuantumRegister, Aer, QuantumCircuit, execute, IBMQ
from qiskit.providers.aer import noise
from warnings import filterwarnings
import os
import numpy as np

filterwarnings("ignore")
MY_TOKEN = os.environ['MY_TOKEN']
IBMQ.save_account(MY_TOKEN)
IBMQ.load_account()
provider = IBMQ.get_provider(group='open')

def qctxbank(money):

    device = provider.get_backend('ibmqx2')
    properties = device.properties()
    noise_model = noise.device.basic_device_noise_model(properties)
    basis_gates = noise_model.basis_gates
    backend = Aer.get_backend('qasm_simulator')
    bank = []
    risk = 0.1
    for i in range(5):
        q = QuantumRegister(1)
        c = ClassicalRegister(1)
        circuit = QuantumCircuit(q, c)
        coupling_map=[0, i + 1]
        circuit.h(q)
        circuit.measure(q, c)
        job_sim = execute(circuit, backend,
                  coupling_map=coupling_map,
                  noise_model=noise_model,
                  basis_gates=basis_gates)
        sim_result = job_sim.result()
        QT = sim_result.get_counts(circuit)
        bank.append(abs(QT['1']-QT['0']) - risk * money[i])
    print(bank)
    '''for j in range(5):
        bank[j] = max([0, bank[j]])'''
    return np.argsort(bank)

f = qctxbank([1.,2.,3.,4.,0.])
print(f)

'''f = open("quantum.txt","w")
f.write(str(bank))
f.close()'''
