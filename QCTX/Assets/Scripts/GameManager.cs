﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public string resultFile;

	public Text globalLabel;
	public Text bankLabel;
	public GameObject resultPanel;
	public Text resultLabel;

	private int playerCount = 0;
	private Bank[] banks;
	private int totalMoney;

	private enum State
	{
		Bet,
		Simulate,
		Finished
	}
	private State state = State.Bet;

	void Start()
	{
		banks = GetComponentsInChildren<Bank>();
		foreach (var bank in banks)
		{
			bank.OnBet += OnBet;

			bank.OnSelect += (b) =>
			{
				bankLabel.text = bank.name;
			};

			bank.OnDeselect += (b) =>
			{
				bankLabel.text = "";
			};
		}
	}

	void Update()
	{
		switch (state)
		{
			case State.Bet:
				globalLabel.text = "betting phase!";
				if (Input.GetButtonDown("Submit"))
				{
					state = State.Simulate;
					StartCoroutine("SubmitBets");
				}
				break;
			case State.Finished:
				if (Input.GetButtonDown("Submit"))
				{
					SceneManager.LoadScene("GameScene");
				}
				break;
		}
	}

	private void OnBet(Bank bank)
	{
		playerCount++;
		totalMoney += 10;
	}

	IEnumerator SubmitBets()
	{
		var msg = "submit bets:\n";
		foreach (var bank in banks)
		{
			msg = bank + " " + bank.Money;
			bank.EndBet();
		}
		Debug.Log(msg);

		globalLabel.text = "Simulating!";
		bankLabel.text = "";

		int[] result;

		try
		{
			result = GetResultFromFile(resultFile);
		}
		catch (System.Exception ex)
		{
			Debug.Log(ex.Message);
			globalLabel.text = ex.Message;
			result = null;
		}

		if (result == null)
		{
			yield return null;
		}
		else
		{
			yield return new WaitForSeconds(Random.Range(1f, 2f));

			for (var i = 0; i < banks.Length - 1; i++)
			{
				banks[result[i]].Collapse();
				if (i < banks.Length - 2)
				{
					yield return new WaitForSeconds(Random.Range(2f, 6f));
				}					
			}

			yield return new WaitForSeconds(2f);

			var bank = banks[result[banks.Length - 1]];
			int moneyPerPlayer = 0;
			if (bank.PlayerCount > 0)
			{
				moneyPerPlayer = (int)(totalMoney / playerCount);
			}
			resultLabel.gameObject.SetActive(true);
			resultPanel.SetActive(true);
			resultLabel.text = bank.name + " is the last standing bank. All the players that bet on this bank each won " + moneyPerPlayer + "$";

			globalLabel.text = "Simulation finished!";
			state = State.Finished;
		}

	}

	private int[] GetResultFromFile(string file)
	{
		string[] lines = System.IO.File.ReadAllLines(file);
		if (lines.Length != 1)
		{
			throw new System.Exception("1 line expected");
		}

		var arrayString = lines[0].Substring(1, lines[0].Length - 2);
		var tokens = arrayString.Split(' ');
		if (tokens.Length != banks.Length)
		{
			throw new System.Exception("Invalid bank count");
		}

		var result = new int[tokens.Length];
		for (var i = 0; i < tokens.Length; i++)
		{
			result[i] = int.Parse(tokens[i]);
			if (result[i] < 0 || result[i] >= tokens.Length)
			{
				throw new System.Exception("Invalid bank id " + result[i]);
			}
		}

		Debug.Log("result: '" + arrayString + "'");
		return result;
	}
}
