﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Bank : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public int id;
	public new string name;
	public int moneyPerBet;
	public Image sprite;
	public Image signSprite;
	public Text moneyLabel;

	public event System.Action<Bank> OnBet;
	public event System.Action<Bank> OnSelect;
	public event System.Action<Bank> OnDeselect;

	public int Money { get; private set; }
	public int PlayerCount { get; private set; }
	private bool canBet = true;

	void Start()
	{
		sprite.color = new Color(0.6f, 0.6f, 0.6f);
		signSprite.color = sprite.color;
	}

	public void EndBet()
	{
		canBet = false;
		sprite.color = new Color(1f, 1f, 1f);
		signSprite.color = sprite.color;
		moneyLabel.enabled = false;
	}

	public void Collapse()
	{
		gameObject.SetActive(false);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (canBet)
		{
			sprite.color = new Color(1f, 1f, 1f);
			signSprite.color = sprite.color;
			OnSelect?.Invoke(this);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (canBet)
		{
			sprite.color = new Color(0.6f, 0.6f, 0.6f);
			signSprite.color = sprite.color;
			OnDeselect?.Invoke(this);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (canBet)
		{
			Money += moneyPerBet;
			PlayerCount++;
			OnBet?.Invoke(this);

			moneyLabel.text = Money + "$";
		}
	}

	public override string ToString()
	{
		return "[Bank name='" + name + "' id=" + id + "]";
	}
}
